const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const ProgressBarWebpackPlugin = require('progress-bar-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
    mode: 'production',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, './dist'),
        publicPath: '/dist/',
        filename: 'datepick.js',
        library: 'datepick-vue',
        libraryTarget: 'umd'
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            }
        ]
    },
    performance: {
        hints: false
    },
    plugins: [
        new CleanWebpackPlugin(),
        new ProgressBarWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),
        new UglifyJSPlugin({
            parallel: true,
            uglifyOptions: {
                mangle: false,
                output: {
                    beautify: true
                }
            }
        }),
        new VueLoaderPlugin()
    ]
}
