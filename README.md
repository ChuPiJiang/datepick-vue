# date-pick-flexend

Watt瓦特工作室，开源贡献，如使用问题可提交issues

## 介绍
从右边向左选择日期，主要用于数据统计、汇总（从结束时间向前选择）

同样的组件，如果是在vue3项目中，请参照这个开源组件包使用 `datepick-vue3` ， 使用vite打包 可以按需使用，同时引入对应的样式使用 

- [https://gitee.com/ChuPiJiang/datepick-vue3#README.md](https://gitee.com/ChuPiJiang/datepick-vue3#README.md) 开源代码仓库

- [npmjs](https://www.npmjs.com/package/datepick-vue3?activeTab=readme) 公网包管理

## Get Started

```
$ npm i date-pick-flexend
```

在`vue2.6+` 版本中使用， `main.js`中添加以下代码
```js
import Vue from 'vue'
import DatepickVue from 'date-pick-flexend'
import 'date-pick-flexend/dist/datepick.css'


Vue.use(DatepickVue)

```

## Usage
---
```html
<template>
  <date-pick-flexend v-model="datetime" type="daterange" placeholder="sss" />
</template>
```


```js
...
  data () {
    return {
      datetime: []
    }
  }
...
```

## ScreenShot
---


## Api
---

待添加

## License
---

Licensed under the MIT License

## Keyword
---
Vue, DatePick, flexend

增加了多个组件，有望做成中台的组件库，直接能给中台管理系统使用
