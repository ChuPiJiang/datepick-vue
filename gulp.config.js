const gulp = require('gulp');
const cleanCSS = require('gulp-clean-css');
const less = require('gulp-less');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');

// 编译less
gulp.task('css', function () {
    return gulp.src('./src/styles/index.less')
        .pipe(less({
            javascriptEnabled: true
        }))
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(rename('datepick.css'))
        .pipe(gulp.dest('./dist'));
});

// 拷贝字体文件
gulp.task('fonts', function () {
    return gulp.src('./src/styles/iconfont/fonts/*.*')
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('default', gulp.series('css', 'fonts'));
