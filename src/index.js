import * as Utils from './utils'

import DatePicker from './picker/date-picker'
import Mixcheck from './mixcheck/index.vue'
import RadioItem from './mixcheck/radio-item/index.vue'
import CheckItem from './mixcheck/check-item/index.vue'
import NoData from './nodata/index.vue'
import Marquee from './marquee/index.vue'

/* 布局组件 */
import Content from './content/index.vue'
import RowForm from './row-form/'

/* 基础组件 */
import Icon from './icon'
import Input from './input'
import Button from './button'
import Dropdown from './dropdown'
import { Col, Row } from './grid/'

export {
  Utils
}

const install = Vue => {
  Vue.component('i-icon', Icon)
  Vue.component('i-input', Input)
  Vue.component('i-input-number', Input.Number)
  Vue.component('i-button', Button)
  Vue.component('i-col', Col)
  Vue.component('i-row', Row)
  Vue.component('i-dropdown', Dropdown)

  Vue.component('i-content', Content)
  Vue.component('i-row-form', RowForm)
  Vue.component('i-row-item', RowForm.Item)

  Vue.component('i-mixcheck', Mixcheck)
  Vue.component('i-radio-item', RadioItem)
  Vue.component('i-check-item', CheckItem)
  Vue.component('i-nodata', NoData)
  Vue.component('i-marquee', Marquee)
  Vue.component('date-pick-flexend', DatePicker)
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export default {
  install
}
